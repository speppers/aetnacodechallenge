package com.main.aetnacodechallenge

import android.widget.AutoCompleteTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.main.aetnacodechallenge.imageSearch.ImageSearchActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class InstrumentedTests {

    @get:Rule
    var activityRule: ActivityTestRule<ImageSearchActivity>
            = ActivityTestRule(ImageSearchActivity::class.java)

    @Test
    fun testSearch() {
        onView(withId(R.id.search)).perform(click())

        onView(isAssignableFrom(AutoCompleteTextView::class.java))
            .perform(typeText("Cats"), closeSoftKeyboard())

        onView(isAssignableFrom(AutoCompleteTextView::class.java))
            .check(matches(withText("Cats")))
    }

}
