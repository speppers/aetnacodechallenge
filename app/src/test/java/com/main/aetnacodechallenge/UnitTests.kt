package com.main.aetnacodechallenge

import com.main.aetnacodechallenge.imageSearch.recentSearch.RecentSearchViewModel
import org.junit.Test

class UnitTests {
    @Test
    fun testRecentSearchDiff() {
        val first = RecentSearchViewModel("Test1")
        val second = RecentSearchViewModel("Test1")
        val third = RecentSearchViewModel("Test2")

        assert(first.areItemsTheSame(second))
        assert(first.areContentsTheSame(second))
        assert(!first.areItemsTheSame(third))
    }
}
