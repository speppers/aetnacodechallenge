package com.main.aetnacodechallenge.room

import androidx.room.TypeConverter
import java.util.Date

class AppTypeConverters {

    @TypeConverter
    fun fromTimestamp(value: Long?) : Date? {
        if (value == null) {
            return null
        }

        return Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?) : Long? {
        return date?.time
    }

}