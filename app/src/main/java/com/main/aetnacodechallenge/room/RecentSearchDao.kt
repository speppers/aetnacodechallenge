package com.main.aetnacodechallenge.room

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
abstract class RecentSearchDao {

    @Query("SELECT * FROM recent_search ORDER BY created_date DESC LIMIT 5")
    abstract fun all(): Flowable<List<RecentSearch>>

    @Query("DELETE FROM recent_search WHERE search NOT IN (SELECT search from recent_search ORDER BY created_date DESC LIMIT 5)")
    abstract fun deleteAllButTop()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(recentSearch: RecentSearch)

    @Transaction
    open fun insertAndClearAllButTop(recentSearch: RecentSearch) {
        insert(recentSearch)
        deleteAllButTop()
    }

}