package com.main.aetnacodechallenge.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "recent_search")
data class RecentSearch(@PrimaryKey
                        @ColumnInfo(name = "search") val search: String,
                        @ColumnInfo(name = "created_date") val createdDate: Date = Date()
)