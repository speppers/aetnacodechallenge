package com.main.aetnacodechallenge.dagger

import com.main.aetnacodechallenge.dagger.modules.RepositoryModule
import com.main.aetnacodechallenge.dagger.modules.RetrofitModule

object Injector {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .repositoryModule(RepositoryModule())
            .retrofitModule(RetrofitModule())
            .build()
    }

}