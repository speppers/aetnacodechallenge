package com.main.aetnacodechallenge.dagger.modules

import com.main.aetnacodechallenge.network.photo.PhotoService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ServiceModule {

    @Provides
    internal fun getPhotoService(retrofit: Retrofit): PhotoService {
        return retrofit.create(PhotoService::class.java)
    }

}