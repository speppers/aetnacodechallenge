package com.main.aetnacodechallenge.dagger.modules

import androidx.room.Room
import com.main.aetnacodechallenge.application.MainApplication
import com.main.aetnacodechallenge.room.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun providesAppDatabase(): AppDatabase {
        return Room.databaseBuilder(
                MainApplication.appContext,
                AppDatabase::class.java, "app_database"
            )
            .fallbackToDestructiveMigration()
            .build()
    }

}