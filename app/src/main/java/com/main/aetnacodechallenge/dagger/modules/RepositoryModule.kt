package com.main.aetnacodechallenge.dagger.modules

import com.main.aetnacodechallenge.network.photo.PhotoRepository
import com.main.aetnacodechallenge.network.photo.PhotoService
import com.main.aetnacodechallenge.network.recentSearch.RecentSearchRepository
import com.main.aetnacodechallenge.room.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesRecentSearchRepository(appDatabase: AppDatabase) : RecentSearchRepository {
        return RecentSearchRepository(appDatabase)
    }

    @Provides
    @Singleton
    fun providesPhotoRepository(photoService: PhotoService, recentSearchRepository: RecentSearchRepository) : PhotoRepository {
        return PhotoRepository(photoService, recentSearchRepository)
    }

}