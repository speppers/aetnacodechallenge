package com.main.aetnacodechallenge.dagger

import com.main.aetnacodechallenge.dagger.modules.DatabaseModule
import com.main.aetnacodechallenge.dagger.modules.RepositoryModule
import com.main.aetnacodechallenge.dagger.modules.RetrofitModule
import com.main.aetnacodechallenge.dagger.modules.ServiceModule
import com.main.aetnacodechallenge.imageSearch.ImageSearchViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [
    RepositoryModule::class,
    RetrofitModule::class,
    ServiceModule::class,
    DatabaseModule::class
])
interface ApplicationComponent {

    fun inject(viewModel: ImageSearchViewModel)

}