package com.main.aetnacodechallenge.application

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho

class MainApplication : Application() {

    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        Stetho.initializeWithDefaults(this)
    }
}