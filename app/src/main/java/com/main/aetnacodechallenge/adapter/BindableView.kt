package com.main.aetnacodechallenge.adapter

interface BindableView<T> {
    fun bind(viewModel: T)
}