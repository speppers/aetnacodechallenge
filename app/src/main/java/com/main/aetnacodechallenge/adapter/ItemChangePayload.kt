package com.main.aetnacodechallenge.adapter

class ItemChangePayload(val oldItems: List<DiffItem>, val newItems: List<DiffItem>)