package com.main.aetnacodechallenge.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class DiffAdapter(delegates: List<AdapterDelegate<MutableList<DiffItem>>>) : ListDelegationAdapter<MutableList<DiffItem>>() {

    init {
        items = ArrayList()
        for (delegate in delegates) {
            delegatesManager.addDelegate(delegate)
        }
    }

    fun update(newItems: List<DiffItem>) {
        val payload = ItemChangePayload(ArrayList(items), newItems)
        val diffResult = DiffUtil.calculateDiff(AdapterDiffCallback(payload))

        items.clear()
        items.addAll(newItems)

        diffResult.dispatchUpdatesTo(this)
    }
}