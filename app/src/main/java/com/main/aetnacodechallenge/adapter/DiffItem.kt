package com.main.aetnacodechallenge.adapter

interface DiffItem {

    fun areItemsTheSame(item: DiffItem): Boolean
    fun areContentsTheSame(item: DiffItem): Boolean

}