package com.main.aetnacodechallenge.imageDetail

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.main.aetnacodechallenge.R
import com.main.aetnacodechallenge.activity.BaseActivity
import com.main.aetnacodechallenge.databinding.ActivityImageDetailBinding
import com.main.aetnacodechallenge.network.photo.models.Photo
import com.main.aetnacodechallenge.utils.Constants

class ImageDetailActivity : BaseActivity() {

    private lateinit var binding: ActivityImageDetailBinding
    private lateinit var viewModel: ImageDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_image_detail) as ActivityImageDetailBinding
        setupViewModel()
        setupToolbar()
    }

    private fun setupViewModel() {
        val photo = intent.getParcelableExtra<Photo>(Constants.PHOTO)
        viewModel = ViewModelProvider(this, ImageDetailViewModel.Factory(photo)).get(ImageDetailViewModel::class.java)
        binding.viewModel = viewModel
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

}