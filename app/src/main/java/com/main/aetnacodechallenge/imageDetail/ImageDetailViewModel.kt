package com.main.aetnacodechallenge.imageDetail

import androidx.databinding.Bindable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.main.aetnacodechallenge.network.photo.models.Photo
import com.main.aetnacodechallenge.viewModel.ViewModelBase

class ImageDetailViewModel(private val photo: Photo?) : ViewModelBase() {

    class Factory(private val photo: Photo?) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ImageDetailViewModel(photo) as T
        }
    }

    val image: String?
        @Bindable get() {
            return photo?.media?.m
        }

    val title: String?
        @Bindable get() {
            return photo?.title
        }

    val description: String?
        @Bindable get() {
            return photo?.description
        }

    val author: String?
        @Bindable get() {
            return photo?.author
        }

    /** Didn't see these properties in the response so they've been left out. */
//    val width: String?
//        @Bindable get() {
//            return photo.width
//        }
//
//    val height: String?
//        @Bindable get() {
//            return photo.height
//        }

}