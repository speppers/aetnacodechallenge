package com.main.aetnacodechallenge.imageSearch

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.main.aetnacodechallenge.R
import com.main.aetnacodechallenge.activity.BaseActivity
import com.main.aetnacodechallenge.adapter.AdapterDelegateFactory
import com.main.aetnacodechallenge.adapter.DiffAdapter
import com.main.aetnacodechallenge.databinding.ActivityImageSearchBinding
import com.main.aetnacodechallenge.extensions.disposedBy
import com.main.aetnacodechallenge.extensions.observeOnMain
import com.main.aetnacodechallenge.imageSearch.photo.PhotoView
import com.main.aetnacodechallenge.imageSearch.photo.PhotoViewModel
import com.main.aetnacodechallenge.imageSearch.recentSearch.RecentSearchView
import com.main.aetnacodechallenge.imageSearch.recentSearch.RecentSearchViewModel
import com.main.aetnacodechallenge.imageSearch.recentSearchHeader.RecentSearchHeaderView
import com.main.aetnacodechallenge.imageSearch.recentSearchHeader.RecentSearchHeaderViewModel
import io.reactivex.disposables.CompositeDisposable

class ImageSearchActivity : BaseActivity() {

    private lateinit var binding: ActivityImageSearchBinding
    private lateinit var viewModel: ImageSearchViewModel
    private val adapter = DiffAdapter(listOf(
        AdapterDelegateFactory.create(PhotoViewModel::class.java, PhotoView::class.java),
        AdapterDelegateFactory.create(RecentSearchViewModel::class.java, RecentSearchView::class.java),
        AdapterDelegateFactory.create(RecentSearchHeaderViewModel::class.java, RecentSearchHeaderView::class.java)
        ))

    private val compositeDisposable = CompositeDisposable()
    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_image_search) as ActivityImageSearchBinding
        viewModel = ViewModelProvider(this).get(ImageSearchViewModel::class.java)
        binding.viewModel = viewModel
        setupRecyclerView()
        setupToolbar()
    }

    private fun setupRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (viewModel.currentState == ImageSearchViewModel.State.RecentSearch) {
                    2
                } else {
                    1
                }
            }
        }

        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onResume() {
        super.onResume()
        viewModel
            .viewModelsObservable
            .observeOnMain()
            .subscribe({
                adapter.update(it)
            }, Throwable::printStackTrace)
            .disposedBy(compositeDisposable)
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu.findItem(R.id.search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isEmpty()) {
                    viewModel.clearSearch()
                } else {
                    viewModel.onSearch(newText)
                }

                return true
            }
        })

        this.searchView = searchView

        return super.onCreateOptionsMenu(menu)
    }

    override fun onBackPressed() {
        searchView?.let { searchView ->
            if (!searchView.isIconified) {
                searchView.isIconified = true
                searchView.onActionViewCollapsed()
                return
            }
        }

        if (viewModel.currentState == ImageSearchViewModel.State.Search) {
            viewModel.clearSearch()
            return
        }

        super.onBackPressed()
    }
}
