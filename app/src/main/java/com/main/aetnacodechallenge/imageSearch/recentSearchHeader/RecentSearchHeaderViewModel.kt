package com.main.aetnacodechallenge.imageSearch.recentSearchHeader

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.main.aetnacodechallenge.adapter.DiffItem

class RecentSearchHeaderViewModel(@Bindable val text: String) : BaseObservable(), DiffItem {

    override fun areItemsTheSame(item: DiffItem): Boolean {
        return item is RecentSearchHeaderViewModel
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        item as RecentSearchHeaderViewModel
        return item.text == text
    }

}