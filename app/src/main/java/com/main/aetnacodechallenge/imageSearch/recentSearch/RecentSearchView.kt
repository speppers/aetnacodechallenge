package com.main.aetnacodechallenge.imageSearch.recentSearch

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.main.aetnacodechallenge.adapter.BindableView
import com.main.aetnacodechallenge.databinding.ViewRecentSearchBinding

class RecentSearchView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), BindableView<RecentSearchViewModel> {

    private val binding: ViewRecentSearchBinding = ViewRecentSearchBinding.inflate(LayoutInflater.from(context), this, true)
    private var viewModel: RecentSearchViewModel? = null

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        setOnClickListener {
            val viewModel = viewModel ?: return@setOnClickListener
            viewModel.onSelect?.invoke(viewModel)
        }
    }

    override fun bind(viewModel: RecentSearchViewModel) {
        this.viewModel = viewModel
        binding.viewModel = viewModel
        binding.executePendingBindings()
    }

}