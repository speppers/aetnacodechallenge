package com.main.aetnacodechallenge.imageSearch.recentSearchHeader

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.main.aetnacodechallenge.adapter.BindableView
import com.main.aetnacodechallenge.databinding.ViewRecentSearchHeaderBinding

class RecentSearchHeaderView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), BindableView<RecentSearchHeaderViewModel> {

    private val binding: ViewRecentSearchHeaderBinding = ViewRecentSearchHeaderBinding.inflate(LayoutInflater.from(context), this, true)
    private var viewModel: RecentSearchHeaderViewModel? = null

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun bind(viewModel: RecentSearchHeaderViewModel) {
        this.viewModel = viewModel
        binding.viewModel = viewModel
        binding.executePendingBindings()
    }

}