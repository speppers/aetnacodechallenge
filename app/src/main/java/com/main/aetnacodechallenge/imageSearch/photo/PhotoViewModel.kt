package com.main.aetnacodechallenge.imageSearch.photo

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.main.aetnacodechallenge.adapter.DiffItem
import com.main.aetnacodechallenge.network.photo.models.Photo

class PhotoViewModel(val photo: Photo) : BaseObservable(), DiffItem {

    val image: String?
        @Bindable get() {
            return photo.media?.m
        }

    val label: String?
        @Bindable get() {
            return photo.title
        }

    override fun areItemsTheSame(item: DiffItem): Boolean {
        if (item !is PhotoViewModel) {
            return false
        }

        return item.image == image
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        item as PhotoViewModel
        return item.label == label
    }

}