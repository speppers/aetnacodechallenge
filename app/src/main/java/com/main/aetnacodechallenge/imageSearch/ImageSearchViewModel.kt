package com.main.aetnacodechallenge.imageSearch

import androidx.databinding.Bindable
import com.main.aetnacodechallenge.BR
import com.main.aetnacodechallenge.R
import com.main.aetnacodechallenge.adapter.DiffItem
import com.main.aetnacodechallenge.application.MainApplication
import com.main.aetnacodechallenge.dagger.Injector
import com.main.aetnacodechallenge.extensions.disposedBy
import com.main.aetnacodechallenge.extensions.observeOnMain
import com.main.aetnacodechallenge.imageSearch.photo.PhotoViewModel
import com.main.aetnacodechallenge.imageSearch.recentSearch.RecentSearchViewModel
import com.main.aetnacodechallenge.imageSearch.recentSearchHeader.RecentSearchHeaderViewModel
import com.main.aetnacodechallenge.network.photo.PhotoRepository
import com.main.aetnacodechallenge.network.recentSearch.RecentSearchRepository
import com.main.aetnacodechallenge.viewModel.ViewModelBase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ImageSearchViewModel : ViewModelBase() {

    enum class State {
        Search,
        RecentSearch
    }

    @Inject lateinit var photoRepository: PhotoRepository
    @Inject lateinit var recentSearchRepository: RecentSearchRepository

    private val searchSubject: PublishSubject<String> = PublishSubject.create()
    private val viewModelsSubject: BehaviorSubject<List<DiffItem>> = BehaviorSubject.createDefault(listOf())
    val viewModelsObservable: Observable<List<DiffItem>> = viewModelsSubject.hide()

    private var recentSearches: List<RecentSearchViewModel> = listOf()
    private var compositeDisposable = CompositeDisposable()
    var currentState: State = State.RecentSearch
        private set

    @Bindable var loading = false
        private set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    init {
        Injector.component.inject(this)

        setupSearchObservable()
        setupRecentSearchObservable()

        updateViewModels()
    }

    private fun setupSearchObservable() {
        searchSubject
            .debounce(300, TimeUnit.MILLISECONDS)
            .filter { it.length > 2 }
            .doOnNext {
                loading = true
            }
            .flatMap {
                photoRepository.get(it)
                    .toObservable()
                    .onErrorReturn {
                        listOf()
                    }
            }
            .map {
                it.map { photo ->
                    PhotoViewModel(photo)
                }
            }
            .observeOnMain()
            .subscribe({
                updateViewModels(it)
                loading = false
            }, {
                loading = false
                it.printStackTrace()
            })
            .disposedBy(compositeDisposable)
    }

    private fun setupRecentSearchObservable() {
        recentSearchRepository.recentSearches
            .debounce(50, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ searches ->
                recentSearches = searches.map {
                    RecentSearchViewModel(it.search, this::onRecentSearchSelected)
                }

                if (currentState == State.RecentSearch) {
                    updateViewModels()
                }
            }, Throwable::printStackTrace)
            .disposedBy(compositeDisposable)
    }

    private fun updateViewModels(photoViewModels: List<PhotoViewModel> = listOf()) {
        if (photoViewModels.isNotEmpty()) {
            viewModelsSubject.onNext(photoViewModels)
            currentState = State.Search
            return
        }

        currentState = State.RecentSearch
        val viewModels = mutableListOf<DiffItem>()
        val headerText = if (recentSearches.isEmpty()) {
            MainApplication.appContext.resources.getString(R.string.no_searches)
        } else {
            MainApplication.appContext.resources.getString(R.string.recent_searches)
        }

        viewModels.add(RecentSearchHeaderViewModel(headerText))
        viewModels.addAll(recentSearches)
        viewModelsSubject.onNext(viewModels)
    }

    private fun onRecentSearchSelected(recentSearchViewModel: RecentSearchViewModel) {
        onSearch(recentSearchViewModel.searchText)
    }

    fun onSearch(searchText: String) {
        searchSubject.onNext(searchText)
    }

    fun clearSearch() {
        updateViewModels()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}