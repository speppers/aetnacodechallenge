package com.main.aetnacodechallenge.imageSearch.photo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.main.aetnacodechallenge.adapter.BindableView
import com.main.aetnacodechallenge.databinding.ViewPhotoBinding
import com.main.aetnacodechallenge.imageDetail.ImageDetailActivity
import com.main.aetnacodechallenge.utils.Constants

class PhotoView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), BindableView<PhotoViewModel> {

    private val binding: ViewPhotoBinding = ViewPhotoBinding.inflate(LayoutInflater.from(context), this, true)
    private var viewModel: PhotoViewModel? = null

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        setOnClickListener {
            val viewModel = viewModel ?: return@setOnClickListener
            val intent = Intent(context, ImageDetailActivity::class.java)
            intent.putExtra(Constants.PHOTO, viewModel.photo)
            context.startActivity(intent)
        }
    }

    override fun bind(viewModel: PhotoViewModel) {
        this.viewModel = viewModel
        binding.viewModel = viewModel
        binding.executePendingBindings()
    }

}
