package com.main.aetnacodechallenge.imageSearch.recentSearch

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.main.aetnacodechallenge.adapter.DiffItem

class RecentSearchViewModel(@Bindable val searchText: String,
                            val onSelect: ((RecentSearchViewModel) -> Unit)? = null) : BaseObservable(), DiffItem {

    override fun areItemsTheSame(item: DiffItem): Boolean {
        if (item !is RecentSearchViewModel) {
            return false
        }

        return item.searchText == searchText
    }

    override fun areContentsTheSame(item: DiffItem): Boolean {
        return true
    }
    
}