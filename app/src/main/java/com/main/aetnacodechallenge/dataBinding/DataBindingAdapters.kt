package com.main.aetnacodechallenge.dataBinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("android:src")
fun setImageSrc(imageView: ImageView, url: String?) {
    if (url == null || url.isEmpty()) {
        imageView.setImageResource(0)
        return
    }

    Glide.with(imageView.context)
        .load(url)
        .into(imageView)
}