package com.main.aetnacodechallenge.network.photo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    val title: String?,
    val link: String?,
    val description: String?,
    val author: String?,
    val authorId: String?,
    val media: Media?,
    val tags: String?) : Parcelable