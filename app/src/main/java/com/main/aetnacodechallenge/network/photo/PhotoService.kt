package com.main.aetnacodechallenge.network.photo

import com.main.aetnacodechallenge.network.photo.models.PhotoResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoService {

    @GET("photos_public.gne?nojsoncallback=1&tagmode=any&format=json")
    fun search(@Query("tags") input: String) : Single<PhotoResponse>

}