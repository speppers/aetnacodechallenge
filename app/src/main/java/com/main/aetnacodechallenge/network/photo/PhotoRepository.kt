package com.main.aetnacodechallenge.network.photo

import com.google.common.cache.CacheBuilder
import com.main.aetnacodechallenge.extensions.subscribeOnIO
import com.main.aetnacodechallenge.network.photo.models.Photo
import com.main.aetnacodechallenge.network.recentSearch.RecentSearchRepository
import io.reactivex.Single

class PhotoRepository(private val photoService: PhotoService,
                      private val recentSearchRepository: RecentSearchRepository) {

    private val cache = CacheBuilder.newBuilder()
        .maximumSize(100)
        .build<String, List<Photo>>()

    fun get(searchText: String) : Single<List<Photo>> {
        val cached = cache.getIfPresent(searchText)
        if (cached != null) {
            recentSearchRepository.add(searchText)
            return Single.just(cached)
        }

        return photoService
            .search(searchText)
            .doOnError(Throwable::printStackTrace)
            .map { it.items }
            .subscribeOnIO()
            .doOnSuccess {
                cache.put(searchText, it)
                recentSearchRepository.add(searchText)
            }
    }

}