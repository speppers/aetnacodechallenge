package com.main.aetnacodechallenge.network.recentSearch

import com.main.aetnacodechallenge.extensions.disposedBy
import com.main.aetnacodechallenge.room.AppDatabase
import com.main.aetnacodechallenge.room.RecentSearch
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RecentSearchRepository(private val database: AppDatabase) {

    private val compositeDisposable = CompositeDisposable()

    val recentSearches : Flowable<List<RecentSearch>>
        get() {
            return database.recentSearchDao()
                .all()
                .subscribeOn(Schedulers.io())
        }

    fun add(searchText: String) {
        Observable.just(database)
            .subscribeOn(Schedulers.io())
            .subscribe { db ->
                db.recentSearchDao().insertAndClearAllButTop(RecentSearch(searchText))
            }
            .disposedBy(compositeDisposable)
    }

}