package com.main.aetnacodechallenge.network.photo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Media(val m: String) : Parcelable