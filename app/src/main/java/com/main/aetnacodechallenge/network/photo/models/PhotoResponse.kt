package com.main.aetnacodechallenge.network.photo.models

data class PhotoResponse(
    val title: String?,
    val link: String?,
    val description: String?,
    val generator: String?,
    val items: List<Photo>)